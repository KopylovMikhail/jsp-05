package ru.kopylov.tm.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.kopylov.tm.entity.Role;
import ru.kopylov.tm.entity.User;
import ru.kopylov.tm.enumerated.RoleType;
import ru.kopylov.tm.repository.UserRepository;

import javax.annotation.PostConstruct;
import java.util.List;

@Service
@Transactional
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @PostConstruct
    private void init() {
        initUser("test", "test", RoleType.ADMINISTRATOR);
        initUser("user", "user", RoleType.USER);
    }

    private void initUser(final String login, final String password, RoleType roleType) {
        User user = userRepository.findByLogin(login);
        if (user != null) return;
        user = new User();
        user.setLogin(login);
        user.setPasswordHash(passwordEncoder.encode(password));
        final Role role = new Role();
        role.setUser(user);
        role.setRoleType(roleType);
        user.getRoles().add(role);
        userRepository.save(user);
    }

    public User save(User user) {
        return userRepository.save(user);
    }

    public User save(final String login, final String password) {
        if (login == null || login.isEmpty() || password == null || password.isEmpty()) return null;
        if (findByLogin(login) != null) return null;
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(passwordEncoder.encode(password));
        final Role role = new Role();
        role.setUser(user);
        role.setRoleType(RoleType.USER);
        user.getRoles().add(role);
        return userRepository.save(user);
    }

    public User findByLogin(final String login) {
        if (login == null || login.isEmpty()) return null;
        return userRepository.findByLogin(login);
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }

}
