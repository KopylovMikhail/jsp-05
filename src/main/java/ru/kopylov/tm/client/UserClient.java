package ru.kopylov.tm.client;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import ru.kopylov.tm.entity.User;

//@FeignClient("user")
public interface UserClient {

//    static UserClient client(final String baseUrl) {
//        final FormHttpMessageConverter converter = new FormHttpMessageConverter();
//        final HttpMessageConverters converters = new HttpMessageConverters(converter);
//        final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
//        return Feign.builder()
//                .contract(new SpringMvcContract())
//                .encoder(new SpringEncoder(objectFactory))
//                .decoder(new SpringDecoder(objectFactory))
//                .target(UserClient.class, baseUrl);
//    }

    @PostMapping(value = "/rest/login", produces = MediaType.APPLICATION_JSON_VALUE)
    String login(User user);

    @PostMapping(value = "/rest/login", produces = MediaType.APPLICATION_XML_VALUE)
    String loginXml(User user);

    @PostMapping(value = "/rest/logout", produces = MediaType.APPLICATION_JSON_VALUE)
    void logout(@RequestHeader("cookie") String sessionId);

    @PostMapping(value = "/rest/logout", produces = MediaType.APPLICATION_XML_VALUE)
    void logoutXml(@RequestHeader("cookie") String sessionId);

}
