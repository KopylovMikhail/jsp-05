package ru.kopylov.tm.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.kopylov.tm.api.service.IProjectService;
import ru.kopylov.tm.entity.Project;

import java.util.List;

@RestController
public class ProjectRestController {

    @Autowired
    IProjectService projectService;

    @RequestMapping(value = "/projects", method = RequestMethod.GET,
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public List<Project> getProjects() {
        return projectService.findAll();
    }

    @GetMapping(value = "/project/{id}",
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public Project getProject(@PathVariable("id") String id) {
        return projectService.findById(id);
    }

    @PutMapping(value = "/project",
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public Project updateProject(@RequestBody Project project) {
        projectService.save(project);
        return project;
    }

    @DeleteMapping(value = "/project/{id}",
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public void deleteProject(@PathVariable("id") String id) {
        projectService.deleteById(id);
    }

    @PostMapping(value = "/project",
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public Project addProject(@RequestBody Project project) {
        projectService.save(project);
        return project;
    }

}
