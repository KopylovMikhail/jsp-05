<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>PROJECTS</title>
</head>
<body>
<h2><a href="/index.jsp">MAIN</a><jsp:text> | </jsp:text><a href="/project_list">PROJECTS</a><jsp:text> | </jsp:text><a href="/task_list">TASKS</a></h2>
<div align="center">
    <h2>TASK VIEW</h2>
    <table border="1" cellpadding="5">
        <tr>
            <th colspan="2">TASK</th>
        </tr>
        <tr>
            <th>ID</th>
            <th>${task.id}</th>
        </tr>
        <tr>
            <th>Project name</th>
            <th>${task.project.name}</th>
        </tr>
        <tr>
            <th>Name</th>
            <th>${task.name}</th>
        </tr>
        <tr>
            <th>Description</th>
            <th>${task.description}</th>
        </tr>
        <tr>
            <th>Date start</th>
            <th>${task.dateStart}</th>
        </tr>
        <tr>
            <th>Date finish</th>
            <th>${task.dateFinish}</th>
        </tr>
        <tr>
            <th>State</th>
            <th>${task.state}</th>
        </tr>
        <tr>
            <th colspan="2">
<%--                <a href="/project_edit/${project.id}">Edit</a>--%>
                <button><a href="/task_edit/${task.id}">Edit</a></button>
            </th>
        </tr>
    </table>
</div>
</body>
</html>
