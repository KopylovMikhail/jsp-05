<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>PROJECTS</title>
</head>
<body>
<h2><a href="/index.jsp">MAIN</a><jsp:text> | </jsp:text><a href="/project_list">PROJECTS</a><jsp:text> | </jsp:text><a href="/task_list">TASKS</a></h2>
    <div align="center">
        <h2>PROJECT MANAGEMENT</h2>
        <table border="1" cellpadding="5">
            <tr>
                <th colspan="7">PROJECTS</th>
            </tr>
            <tr>
                <th>Nr</th>
                <th>ID</th>
                <th>Name</th>
                <th>Description</th>
                <th>View</th>
                <th>Edit</th>
                <th>Remove</th>
            </tr>
            <c:forEach items="${projects}" var="project">
                <tr>
                    <td></td>
                    <td>${project.id}</td>
                    <td>${project.name}</td>
                    <td>${project.description}</td>
                    <td>
                        <a href="/project_view/${project.id}">View</a>
                    </td>
                    <td>
                        <a href="/project_edit/${project.id}">Edit</a>
                    </td>
                    <td>
                        <a href="/project_remove/${project.id}">Remove</a>
                    </td>
                </tr>
            </c:forEach>
        </table>
        <h4><a href="/project_create">CREATE PROJECT</a></h4>
    </div>
</body>
</html>
